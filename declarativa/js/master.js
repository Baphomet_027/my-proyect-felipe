window.addEventListener('load', (e) => {

  let sectionElemts = document.querySelectorAll('section')

  let sections = []
  for (let i = 0; i < sectionElemts.length; i++)
    sections.push(new Section(sectionElemts[i]))

console.log(sections);

  function Section(root) {
    let that = this

    this.isOpen = false
    this.root = root
    this.icon = root.querySelector('.icon')
    this.content = root.querySelector('.content')

    this.toggle = () => {
      this.isOpen = !this.isOpen
      updateIcon()
      updateContent()
    }


    this.root.addEventListener('click', (e) => {
      this.toggle()
    })



    function updateIcon() {
      if (that.isOpen) {
        that.icon.classList.add('up')
        that.icon.classList.remove('down')
      }
      else {
        that.icon.classList.add('down')
        that.icon.classList.remove('up')
      }
    }

    function updateContent() {
      if (that.isOpen) {
        that.content.style.display = 'block'
      }
      else {
        that.content.style.display = 'none'
      }
    }
  }

})
