window.addEventListener('load', (e) => {
  let video = document.querySelector('video')

  let btnPlay = document.querySelector('#btn-play')
  let btnStop = document.querySelector('#btn-stop')

  btnPlay.addEventListener('click', (e) => {
    video.currentTime = 0
      video.play()
  })

  btnStop.addEventListener('click', (e) => {
      video.pause()

  })

})
