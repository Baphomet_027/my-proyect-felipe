window.addEventListener('load', (e) => {

  let template = document.querySelector('#title')
  let app = document.querySelector('#app')

  let clone = document.importNode(template.content, true)
  app.appendChild(clone)
})
