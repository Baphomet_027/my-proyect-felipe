
window.addEventListener('load', (e) => {

  function login() {
    let form = document.querySelector('#login form')
    form.addEventListener('submit', (e) => {
      e.preventDefault()

      let txtUser = document.querySelector('[name=user]')
      let txtPass = document.querySelector('[name=pass]')

      let data = {
        user: txtUser.value,
        pass: txtPass.value
      }

      console.log(data)
    })
  }

  function register() {
    let form = document.querySelector('#register form')
    form.addEventListener('submit', (e) => {
      e.preventDefault()

      let optSex = document.querySelectorAll('[name=sex]')
      // console.log(optSex)
      // console.log(optSex[0].checked)

      let txtEmail = document.querySelector('[name=email]')
      let txtVerifyEmail = document.querySelector('[name=verify-email]')


      console.log('email : ' + validateEmail(txtEmail.value));
      console.log('verify email : ' + validateEmail(txtVerifyEmail.value));


      if (txtEmail.value == txtVerifyEmail.value)
        console.log('correos iguales')
      else
        console.log('correos NO iguales')
    })





    function validateEmail(email) {
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
  }

  function comment() {
    let form = document.querySelector('#comment form')
    form.addEventListener('submit', (e) => {
      e.preventDefault()

      // let textarea = document.querySelector('textarea')
      // console.log(textarea.value);
    })

    let selectCat = document.querySelector('[name=categories]')
    // console.log(selectCat)
    // console.log(selectCat.value);

    selectCat.addEventListener('change', (e) => {
      console.log(e);
      console.log(e.target.value);
    })

    let opt = selectCat.querySelector('option[selected]')
    opt.removeAttribute('selected')

    let opts = selectCat.querySelectorAll('option')
    console.log(opts[2]);
    opts[2].setAttribute('selected', true)

    // let opt = selectCat.querySelector('option[selected]')
    // console.log(opt);
  }



  login()
  register()
  comment()
})
