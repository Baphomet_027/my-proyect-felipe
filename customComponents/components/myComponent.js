
class MyComponent extends HTMLElement {
  constructor() {
    super()
  }

  connectedCallback() {
    let title = this.getAttribute('title')
    let subtitle = this.getAttribute('subtitle')

    // let html = '<h1>'+title+'</h1>'
    // html += '<h2>'+subtitle+'</h2>'
    // this.innerHTML = html

    let template = `
    <h1>${title + ' - '}</h1>
    <h2>${subtitle}</h2>
    `
    this.innerHTML = template

  }
}

customElements.define('my-component', MyComponent)
