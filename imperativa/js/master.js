window.addEventListener('load', (e) => {

  let sections = document.querySelectorAll('section')

  for (let i = 0; i < sections.length; i++) {
    sections[i].addEventListener('click', (e) => {
      let icon = sections[i].querySelector('.icon')
      let content = sections[i].querySelector('.content')

      if(sections[i].classList.contains('open')) {
        sections[i].classList.remove('open')

        icon.classList.add('down')
        icon.classList.remove('up')

        content.style.display = 'none'
      }
      else {
        sections[i].classList.add('open')

        icon.classList.add('up')
        icon.classList.remove('down')

        content.style.display = 'block'
      }
    })

  }

})
