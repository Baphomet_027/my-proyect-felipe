window.addEventListener('load', (e) => {
  let slcDptos = document.querySelector('#dptos')
  let slcProvs = document.querySelector('#provs')
  let slcDistrs = document.querySelector('#distrs')


  slcDptos.addEventListener('change', (e) => {
    fillProvs(slcDptos.value)
  })

  slcProvs.addEventListener('change', (e) => {
    fillDistrs(slcDptos.value, slcProvs.value)
  })


  let db = null
  fetch('./db')
    .then(response => response.json())
    .then((data) => {
      db = data

      fillDptos()
    })


    function fillDptos() {
      slcDptos.innerHTML = '<option>Departamento</option>'
      for (let dpto in db) {
        if (db.hasOwnProperty(dpto)) {
          slcDptos.innerHTML += '<option value="'+dpto+'">'+dpto+'</option>'
        }
      }
    }

    function fillProvs(dpto) {
      slcProvs.innerHTML = '<option>Provincia</option>'
      for (let prov in db[dpto]) {
        if (db[dpto].hasOwnProperty(prov)) {
          slcProvs.innerHTML += '<option value="'+prov+'">'+prov+'</option>'
        }
      }
    }

    function fillDistrs(dpto, prov) {
      slcDistrs.innerHTML = '<option>Provincia</option>'
      for (let dtr in db[dpto][prov]) {
        if (db[dpto][prov].hasOwnProperty(dtr)) {
          slcDistrs.innerHTML += '<option value="'+db[dpto][prov][dtr]+'">'+db[dpto][prov][dtr]+'</option>'
        }
      }
    }

})
