class Animal {
  constructor(nombre) {
    console.log('Animal - constructor')
    this.nombre = nombre
  }
}

class Gato extends Animal {
  constructor(nombre) {
    super(nombre) // constructor de Animal
    console.log('Gato - constructor')
  }

  MostrarNombre() {
    console.log(this.nombre)
  }
}

class Perro extends Animal {
  constructor(nombre) {
    super(nombre) // constructor de Animal
    console.log('Perro - constructor')
  }
}

let myCat = new Gato('michi')
myCat.MostrarNombre()

let myDog = new Perro('fido')
console.log(myDog.nombre)
