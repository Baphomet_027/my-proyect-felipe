window.addEventListener('load', (e) => {
  let button = document.querySelector('button')
  button.addEventListener('click', (e) => {
    let xhr = new XMLHttpRequest()
    // xhr.open('post', './info.html')

    // xhr.addEventListener('load', (e) => {
    //   // console.log(e.target.response);
    //   let data = e.target.response
    //
    //   let p = document.querySelector('p')
    //   p.innerText = data
    // })

    xhr.open('get', 'https://jsonplaceholder.typicode.com/todos/')
    xhr.addEventListener('load', (e) => {
      let response = e.target.response
      let json = JSON.parse(response)
      console.log(json);

      let ul = document.querySelector('ul')
      for (let userId in json) {
        console.log(json[userId]);
        ul.innerHTML += '<li>'+json[userId].title+'</li>'
      }
      
    })

    xhr.send()
  })
})
