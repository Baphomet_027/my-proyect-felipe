window.addEventListener('load', (e) => {
  let button = document.querySelector('button')
  button.addEventListener('click', (e) => {
    fetch('https://jsonplaceholder.typicode.com/todos/')
      .then(response => response.json())
      .then((data) => {
        console.log(data);
      })
  })
})
