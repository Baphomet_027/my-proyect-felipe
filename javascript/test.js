var a = 4
var b = 5

// condiciones
console.log('Condiciones')
console.log('===========')
if (a < b) {
  console.log('a es menor que b')
}
else {
  console.log('b es menor que a')
}
console.log()
console.log()

// loop
console.log('loop - for')
console.log('==========')
for (var i = 0; i < 5; i++) {
  console.log(i)
}
console.log()
console.log()

// postincremento y preincremento
console.log('postincremento')
console.log('==============')
var x = 1
x = x + 1 // x -> 2
x += 1 // x -> 3
x += 5 // x -> 8
x++ // z -> 9
++x // x -> 10

console.log(++x)
console.log('x = ' + x)
