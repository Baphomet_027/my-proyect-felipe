const User = function (name, lastName) {

  let _name = name
  let _lastName = lastName

  function printInfo() {
    console.log('{')
    console.log('  name: ' + _name.toUpperCase())
    console.log('  lastName: ' + _lastName)
    console.log('}')
  }

  return {
    printInfo
  }
}

let user = new User('Maricel', 'Solano')
user.printInfo()
