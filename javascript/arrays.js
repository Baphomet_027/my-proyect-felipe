var myArray = [1, 0, 5, 6]
console.log(myArray)

console.log(myArray[2])
myArray[1] = 6
console.log(myArray)

console.log(myArray.length)
console.log(myArray[myArray.length-1])

for (var i = 0; i < myArray.length; i++) {
  console.log(i + ' -> ' + myArray[i])
}

for (var element in myArray) {
  console.log(element)
}

myArray.forEach((item) => {
  console.log(item)
})

myArray.forEach(printItem)
function printItem (item) {
  console.log(item)
}

myArray.forEach((item, i) => {
  console.log(item, i)
});


myArray.push(8)
console.log(myArray)

var x = myArray.pop();
console.log(x)
console.log(myArray)

var y = myArray.shift();
console.log(y)
console.log(myArray)

var find = myArray.find((element) => {
  return element % 2 == 0
})

console.log(find)


var newArray = myArray.map((element) => {
  return element * 2
})

console.log(newArray)


var newArray = myArray.slice(1,2)
console.log(newArray)
