function Character(name, level) {
  this.name = name
  this.level = level

  this.info = () => {
    // ...
    console.log('name: ' + this.name)
    console.log('level:' + this.level)
  }

}

var maveryk = new Character('maveryk', 10)
console.log(maveryk)
maveryk.info()

var ralba = new Character('ralba', 5)
console.log(ralba)
ralba.info()
