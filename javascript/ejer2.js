// Dibujar la siguiente figura ingresando
// la cantidad de filas. ejem: filas = 4
// *
// **
// ***
// ****

 //   **
 //  ****
 // ******
 //  ****
 //   **

const Shape = function() {
  let row = 4

  function drawTriangle() {
    for (var i = 1; i <= row; i++) {
      let line = ''
      for (var j = 1; j <= i; j++)
        line += '*'

      console.log(line)
    }
  }

  function drawTriangleFlipX() {
    // lineas
    for (var i = 1; i <= row; i++) {
      let line = ''
      // columnas - espacios
      for (var j = 1; j <= row - i; j++)
        line += ' '

      // columnas - *
      for (var k = 1; k <= i; k++)
        line += '*'

      console.log(line)
    }

    // (row - i) + (i) = row
    //
    //     *
    //    **
    //   ***
    //  ****
    // *****
  }

  function drawTriangleFlipY() {
    for (var i = 1; i <= row; i++) {
      let line = ''
      for (var j = row; j >= i; j--)
        line += '*'

      console.log(line)
    }
  }

  function drawTriangleFlipXY() {
    // lineas
    for (var i = row; i >= 1; i--) {
      let line = ''
      // columnas - espacios
      for (var j = 1; j <= row - i; j++)
        line += ' '

      // columnas - *
      for (var k = 1; k <= i; k++)
        line += '*'

      console.log(line)
    }
  }

  return {
    row,
    drawTriangle,
    drawTriangleFlipX,
    drawTriangleFlipY,
    drawTriangleFlipXY
  }
}


let row = 5
let shape = new Shape()
shape.row = row
shape.drawTriangleFlipX()
